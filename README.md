# statsbook

A (hopefully collaborative) book covering the theory required to apply statistics in ecological applications.

Download the latest version [here](https://www.gitlab.com/RTbecard/ibl_experimental_analysis/-/jobs/artifacts/master/raw/Stats-Book.pdf?job=compile_pdf).
